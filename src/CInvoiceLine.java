import java.io.PrintStream;

public class CInvoiceLine extends CElement {
 public CProduct producte;
 public int quantitat;
 
 public CInvoiceLine(CProduct producte,int quantitat) {
  this.producte = producte;
  this.quantitat = quantitat;
 }
 
 public void Print(PrintStream out) {
  out.print("CInvoiceLine(");
  out.print(producte.m_Name);
  out.print(",");
  out.print(quantitat);
  out.print(")");
 }
	public void PrintFactura() {
		System.out.println(quantitat + "\t" + producte.m_Name + "\t" + producte.price + "\t" + producte.price * quantitat);
	}
}