public class CInvoiceLineList extends CList {
	public void PushBack(CInvoiceLine e) {
		super.PushBack(e);
	}
	public float TotalLinies() {
		float totalLinies = 0;
		CNode node = m_Start;
		while (node != null) {
			CInvoiceLine linia = (CInvoiceLine) node.m_Element;
			totalLinies += linia.producte.price * linia.quantitat;
			node = node.m_Next;
		}
		return totalLinies;
	}
	public void PrintLinies() {
		CNode node = m_Start;
		while (node != null) {
			CInvoiceLine linia = (CInvoiceLine) node.m_Element;
			linia.PrintFactura();
			node = node.m_Next;
		}
	}
}