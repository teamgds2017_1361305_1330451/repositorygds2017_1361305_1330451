public class CProductList extends CList {
	public CProduct SearchByName(String name) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CProduct) node.m_Element).m_Name.equalsIgnoreCase(name)) {
				return (CProduct) node.m_Element;				
			}
			node=node.m_Next;
		}
		return null;
	}
	public CProduct SearchByCode(int code) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CProduct) node.m_Element).m_Code==code) {
				return (CProduct) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CProduct e) {
		super.PushBack(e);
	}
	public void PrintProductList() {
		CNode node = m_Start;
		while (node != null) {
			CProduct producte = (CProduct) node.m_Element;
			System.out.println(producte.m_Code + "\t" + producte.m_Name + "\t    " + producte.price);
			node = node.m_Next;
		}

	}
}
